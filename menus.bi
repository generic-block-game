/'
This file is part of Generic Block Game

Generic Block Game is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Generic Block Game is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'/

Type button
	text As String * 64
	As Byte row, column, border
	As UShort x, y, w, h
	keypress As String * 2
End Type


Type buttonset
	As button n(6)
End Type


Function isin(xx As Short,yy As Short,x As Short,y As Short,w As UShort=50,h As UShort=50) As Byte
		Dim in As Byte = True
		in=in And xx>=x And xx<=x+w
		in=in And yy>=y And yy<=y+h
		Return in
End Function



Function readtitle(filename As String) As String
	Dim As String l, result
	Open filename For Input As #1
	If Err>0 Then Print "Error opening the file":End
	Do
		Line Input #1,l
		Select Case l
			Case "Title {"
				Line Input #1,result
		End Select
	Loop Until Eof(1)
	Close #1
	Return result
End Function


Function drawbutton(text As String, keypress As String = "", row As Byte, column As Byte = -1, border As Byte = True, filled As Byte = False) As button
'Draws a button. If column (0 to 4) is present, draws a smaller button in that column
	Const MAXW = 309, H = 40, MARGIN = 10, PL = 20, FH = 8, FW = 8
	Dim as Short w = MAXW, ox = 0
	If column >= 0 Then
		w = MAXW / 4 - MARGIN * 0.75
		ox = (w + MARGIN) * column
	End If
	If filled Then
		Line (ox, H*row + MARGIN*row)-(ox + w, H*row+MARGIN*row+H), RGB(0,0,127), bf
	Else
		Line (ox, H*row + MARGIN*row)-(ox + w, H*row+MARGIN*row+H), RGB(11,11,11), bf
	End If
	If border Then Line (ox, H*row + MARGIN*row)-(ox + w, H*row+MARGIN*row+H), RGB(127,127,127), b
	If (Not border) Or (column >= 0) Then
		Draw String (ox + w/2-Len(text)*FW/2, H*row+MARGIN*row + H/2-FH/2), text
	Else
		Draw String (ox + PL, H*row+MARGIN*row + H/2-FH/2), text
	End If
	Return Type(text, row, column, border, ox, H*row + MARGIN*row, w, H, keypress)
End Function


Function menumouse(buttons() As button) As String
	Dim e As EVENT
	Dim As Integer mousex, mousey, pressed

	If (ScreenEvent(@e)) Then
		Select Case e.type
		Case EVENT_MOUSE_BUTTON_PRESS
			pressed = e.button=1
				If e.button=2 Then Return Chr(27)
				If pressed Then
					GetMouse(mousex,mousey)
					Dim a as UByte
					For a = 0 to UBound(buttons)
						If isin(mousex,mousey,buttons(a).x, buttons(a).y,buttons(a).w,buttons(a).h) Then Return buttons(a).keypress
					Next
				End If
		Case EVENT_MOUSE_BUTTON_RELEASE
			pressed = 0
		Case EVENT_MOUSE_MOVE
			If (pressed) Then
				ScreenControl GET_WINDOW_POS, mousex, mousey
				ScreenControl SET_WINDOW_POS, mousex + e.dx, mousey + e.dy
			End If
		End Select
	End If
End Function


Function drawmenu(title As String, options() As String, selected_button As Byte = 0) As Byte
	'Draws a menu with 4 options from options array 
	Dim keypress As String
	Dim buttons(6) As button
	Dim As UByte page
	page = selected_button \ 4
	selected_button = selected_button Mod 4 + 1
	Dim maxpage As UByte = (UBound(options)) \ 4 + 1
	Do
		resizewindow(310, 300, title) 'resize or cls
		drawbutton(title,,0,,False)

		'write out 4 options
		Dim As Byte row = 1, fn = page * 4
		Do While row <= 4 And fn <= UBound(options)
			buttons(row) = drawbutton(row & ". " + options(fn), Str(row), row,,,row = selected_button)
			row += 1 : fn += 1
		Loop

		buttons(5) = drawbutton("prev", "p", 5, 0)
		buttons(6) = drawbutton("next", "n", 5, 1)
		drawbutton(page + 1 & "/" & maxpage, , 5, 2, False)
		buttons(0) = drawbutton("exit", "x", 5, 3)
		'wait for key or click
		Do
			keypress = InKey
			If keypress = "" Then keypress = menumouse(buttons())
		Loop Until keypress <> ""
		'prev/next page
		Select Case keypress
			Case Chr(255)+"P"
				selected_button += 1
				If selected_button > row - 1 Then selected_button = 1
			Case Chr(255)+"M", "n" 'down or right or n
				page = (page + 1) Mod maxpage
				selected_button = 1
			Case Chr(255)+"H"
				selected_button -= 1
				If selected_button < 1 Then selected_button = row - 1
			Case Chr(255)+"K" , "p"  'up or left or p
				page = (page - 1 + maxpage) Mod maxpage
				selected_button = 1
			Case Chr(13) 'Enter
				If selected_button > 0 Then keypress = Str(selected_button)
				Exit Do
			Case Else
				Exit Do
		End Select
	Loop
	'return selected option + page*4 - 1
	If Val(keypress) >= 1 And Val(keypress) <= 4 And Len(options(Val(keypress) + page*4 - 1)) Then Return Val(keypress) + page*4 - 1
	Return -1
End Function


Function onoff(what As Byte) As String
	If what Then Return ": on"
	Return ": off"
End Function


Sub opentilesettings()

End Sub


Sub opensettings()
	Dim selected As Byte = 0
	Dim title As String = "Settings"
	Dim options(5) As String
	Do
		options(0) = "Show shadow" + onoff(settings.show_shadow)
		options(1) = "Show speed" + onoff(settings.show_speed)
		options(2) = "Show score" + onoff(settings.show_score)
		options(3) = "Play with mouse" + onoff(settings.play_mouse)
		options(4) = "1st button turning" + onoff(settings.first_button_turning)
		options(5) = "Curent tileset: " + tilesfiles(settings.tileset)
		selected = drawmenu(title, options(), selected)
		Select Case selected
			Case 0
				settings.show_shadow = Not settings.show_shadow
			Case 1
				settings.show_speed = Not settings.show_speed
			Case 2
				settings.show_score = Not settings.show_score
			Case 3
				settings.play_mouse = Not settings.play_mouse
			Case 4
				settings.first_button_turning = Not settings.first_button_turning
			Case 5
				loadTiles(drawmenu("Select tileset", tilesfiles(), settings.tileset))
				Exit Do
			Case -1
				Exit Do
		End Select
	Loop
End Sub


Function openloader(directory As String) As String
	Dim title As String = "Load Variation"
	'store all filenames
	ReDim filenames (0) As String 
	filenames(0) = Dir(directory + "/*")
	Do While Len(filenames(UBound(filenames)))
		ReDim Preserve filenames(UBound(filenames) + 1)
		filenames(UBound(filenames)) = Dir()
	Loop
	ReDim Preserve filenames(UBound(filenames) - 1) 'last one was empty
	'extract titles
	Dim titles(UBound(filenames)) As String
	Dim As Byte row = 1, fn = 0
	Do While fn <= UBound(filenames)
		titles(fn) = readtitle(directory + "/" + filenames(fn))
		fn += 1
	Loop
	'draw menu and return a variation (if any)
	fn = drawmenu(title, titles())
	If fn > -1 Then Return directory + "/" + filenames(fn)
End Function


Sub openhelp()
	resizewindow(310,300, "Help")

	Color RGB(127,127,127)
	Print "Generic Block Game"
	Print
	Print "This game is free software (GPL3+)."
	Print "See fbc.bas for source and details."
	Print
	Color RGB(255,255,255)
	If gravity Then
		Print "Use left and right arrows to move,"
		Print "Up arrow or space to rotate"
		Print "Down arrow to lower, Enter to drop,"
	Else
		Print "Use arrows to move,"
		Print "Space to rotate, Enter to fix,"
	End If
	Print
	Print "F1 for help, T to change tileset,"
	Print "S to toggle score, O for options,"
	Print "+ to increase speed, ESC for menu"
	Print
	Color RGB(127,127,127)
	Print "Help for " + game.title
	Color RGB(255,255,255)
	Print

	Dim As UByte a
	For a = 0 To UBound(help)
		Print help(a)
	Next a
	
	Dim e As EVENT
	Do
		keypress=InKey
		ScreenEvent(@e)
		Sleep 25
	Loop Until keypress <> "" Or e.type = EVENT_MOUSE_BUTTON_PRESS

	resizewindow()
	drawplayfield()
End Sub


Sub openmenu()
	Dim selected As Byte = 0
	Dim title As String = "Menu"
	Dim options(4) As String
	Do
		options(0) = "Show help"
		options(1) = "Restart game"
		options(2) = "Load game"
		options(3) = "Quit game"
		options(4) = "Settings"
		selected = drawmenu(title, options(), selected)
		Select Case selected
			Case 0
				openhelp()
				Exit Sub
			Case 1
				gameRestart()
				Exit Do
			Case 2
				If loadgame(openloader(VARPATH)) Then Exit Sub 'Don't restore window size
			Case 3
				quit()
			Case 4
				opensettings()
			Case -1
				Exit Do
		End Select
		Sleep 25
	Loop
	resizewindow()
	drawplayfield()
End Sub
